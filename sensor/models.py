from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Sensor(models.Model):
    user = models.ForeignKey(User, verbose_name="User", null=True,
                                blank=True, related_name="sensor_usuario", on_delete=models.CASCADE)
    latitude = models.FloatField(
        blank=True, null=True, verbose_name='Latitude')
    length = models.FloatField(
        blank=True, null=True, verbose_name='Length')
    imei = models.CharField(
        max_length=15, primary_key=True, verbose_name='IMEI')
    aggregate = models.BooleanField(default=False)
    created = models.DateTimeField(
        auto_now_add=True, verbose_name="Creation date")
    updated = models.DateTimeField(
        auto_now=True, verbose_name="Update date")

    class Meta:
        verbose_name = "Sensor"
        verbose_name_plural = "Sensors"

    def __str__(self):
        return "{}".format(self.imei)



class DataSensor(models.Model):
    sensor = models.OneToOneField(Sensor, verbose_name="Sensor",
                               related_name="datasensor_sensor", on_delete=models.CASCADE)
    db = models.FloatField(verbose_name="Db")
    voltage = models.FloatField(verbose_name="Voltaje")
    humidity_environmental = models.FloatField(
        verbose_name="Humedad ambiental")
    temperature_environmental = models.FloatField(
        verbose_name="Temperatura ambiental")
    ground_one_hum = models.FloatField(verbose_name="Suelo uno Humedad")
    ground_one_tem = models.FloatField(verbose_name="Suelo uno temperatura")
    ground_one_con = models.FloatField(verbose_name="Suelo uno conductividad")
    ground_two_hum = models.FloatField(verbose_name="Suelo dos humedad")
    ground_two_tem = models.FloatField(verbose_name="Suelo dos temperatura")
    ground_two_con = models.FloatField(verbose_name="Suelo dos conductividad")
    sheet_hum = models.FloatField(verbose_name="Hoja Humedad")
    sheet_temp = models.FloatField(verbose_name="Hoja temperatura")
    brightness = models.FloatField(verbose_name="Luminosidad")
    speed_wind = models.FloatField(verbose_name="Velocidad del viento")
    address_wind = models.CharField(
        max_length=10, verbose_name="Direccion del viento")
    radiation_solar = models.FloatField(verbose_name="Radiacion solar")
    created = models.DateTimeField(
        auto_now_add=True, verbose_name="Fecha de creación")

    class Meta:
        verbose_name = "Data Sensor"
        verbose_name_plural = "Data Sensors"

    def __str__(self):
        return "{}".format(self.sensor)
