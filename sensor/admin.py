from django.contrib import admin
from .models import Sensor, DataSensor

# Register your models here.
@admin.register(Sensor)
class SensorAdmin(admin.ModelAdmin):
    list_display = ('imei', 'user')
    search_fields = ('imei', )
    list_filter = ('created', )


@admin.register(DataSensor)
class DataSensorAdmin(admin.ModelAdmin):
    list_display = ('pk', 'sensor', 'created')
    search_fields = ('sensor', )
    list_filter = ('created', )
