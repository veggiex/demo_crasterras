from django.urls import path
from .views import SensorDataApi

urlpatterns = [
    path('data/', SensorDataApi.as_view()),
]
