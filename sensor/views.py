from django.shortcuts import render


# API
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated

# Models
from .models import DataSensor, Sensor

#Serializer
from .serializers import DataSensorSerializer


class SensorDataApi(CreateAPIView):
    permission_classes = (IsAuthenticated,)

    queryset = DataSensor.objects.all()
    serializer_class = DataSensorSerializer
