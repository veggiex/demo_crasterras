# Generated by Django 2.1 on 2019-09-24 19:55

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='DataSensor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('db', models.FloatField(verbose_name='Db')),
                ('voltage', models.FloatField(verbose_name='Voltaje')),
                ('humidity_environmental', models.FloatField(verbose_name='Humedad ambiental')),
                ('temperature_environmental', models.FloatField(verbose_name='Temperatura ambiental')),
                ('ground_one_hum', models.FloatField(verbose_name='Suelo uno Humedad')),
                ('ground_one_tem', models.FloatField(verbose_name='Suelo uno temperatura')),
                ('ground_one_con', models.FloatField(verbose_name='Suelo uno conductividad')),
                ('ground_two_hum', models.FloatField(verbose_name='Suelo dos humedad')),
                ('ground_two_tem', models.FloatField(verbose_name='Suelo dos temperatura')),
                ('ground_two_con', models.FloatField(verbose_name='Suelo dos conductividad')),
                ('sheet_hum', models.FloatField(verbose_name='Hoja Humedad')),
                ('sheet_temp', models.FloatField(verbose_name='Hoja temperatura')),
                ('brightness', models.FloatField(verbose_name='Luminosidad')),
                ('speed_wind', models.FloatField(verbose_name='Velocidad del viento')),
                ('address_wind', models.CharField(max_length=10, verbose_name='Direccion del viento')),
                ('radiation_solar', models.FloatField(verbose_name='Radiacion solar')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')),
            ],
            options={
                'verbose_name': 'Data Sensor',
                'verbose_name_plural': 'Data Sensors',
            },
        ),
        migrations.CreateModel(
            name='Sensor',
            fields=[
                ('latitude', models.FloatField(blank=True, null=True, verbose_name='Latitude')),
                ('length', models.FloatField(blank=True, null=True, verbose_name='Length')),
                ('imei', models.CharField(max_length=15, primary_key=True, serialize=False, verbose_name='IMEI')),
                ('aggregate', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Creation date')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Update date')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='sensor_usuario', to=settings.AUTH_USER_MODEL, verbose_name='User')),
            ],
            options={
                'verbose_name': 'Sensor',
                'verbose_name_plural': 'Sensors',
            },
        ),
        migrations.AddField(
            model_name='datasensor',
            name='sensor',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='datasensor_sensor', to='sensor.Sensor', verbose_name='Sensor'),
        ),
    ]
