from django.shortcuts import render

#libs
from django.views.generic import DetailView, ListView
# model
from .models import Crop


class CropView(ListView):
    model = Crop
    template_name = 'crop/index.html'

