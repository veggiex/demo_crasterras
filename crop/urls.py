from django.urls import path
from .views import CropView

urlpatterns = [
    path('', CropView.as_view(), name="index"),
]
