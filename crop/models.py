from django.db import models

#model sensor
from sensor.models import Sensor
TYPE_OF_CROP = (
    ('CHILE HABANERO', 'Chile habanero'),
    ('PAPAYA', 'Papaya'),
    ('LIMON', 'Limon')
)

# Create your models here.
class Crop(models.Model):
    name = models.CharField(max_length=200, verbose_name="Nombre del cultivo")
    type_crop = models.CharField(max_length=40, choices=TYPE_OF_CROP)
    sensor = models.ForeignKey(
        Sensor, verbose_name="Sensor", related_name="cultivo_sensor", on_delete=models.CASCADE)
    created = models.DateTimeField(
        auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(
        auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = "Crop"
        verbose_name_plural = "Crops"

    def __str__(self):
        return "{} {} {}".format(self.name, self.type_crop, self.created)
