from django.contrib import admin

# models
from .models import Crop

@admin.register(Crop)
class CropAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name', 'type_crop')
    search_fields = ('name', )
    list_filter = ('created', )
